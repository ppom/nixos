{ lib, config, pkgs, ... }:
let
  hostName = config.networking.hostName;
  hosts = builtins.fromTOML (builtins.readFile ../common/hosts.toml);
  host = hosts.${hostName};
  mountName = "musi";
  mountPath = "/${mountName}";
in lib.mkMerge [
  # NFS
  {
    services.nfs.server.enable = true;
    environment.systemPackages = [ pkgs.nfs-utils ];
    boot.kernelModules = [ "nfs" ];
    systemd.mounts = [
      {
        type = "nfs";
        what = "${hosts.musi.address}:/data/akesi";
        where = mountPath;
      }
    ];
  }
  # Transmission
  {
    systemd.services.transmission = {
      after = [ "${mountName}.mount" ];
      requires = [ "${mountName}.mount" ];
    };
    services.transmission = {
      enable = true;
      openPeerPorts = true;
      performanceNetParameters = true;
      # credentialsFile = "/var/secrets/transmission/auth.json";
      settings = {
        incomplete-dir = "${mountPath}/downloading";
        download-dir = "${mountPath}/upload-here";
        watch-dir = "${mountPath}/dot.torrents";
        watch-dir-enabled = true;
        watch-dir-force-generic = true; # NFS shares doesn't support inotify
        speed-limit-up = 1024; # KB/s
        speed-limit-up-enabled = true;
        rpc-bind-address = host.address;
        rpc-username = "ppom";
        # We're already binding the socket to the VPN, so only trusted nodes have access
        rpc-whitelist-enabled = false;
        # rpc-whitelist = "127.0.0.1,${hosts.sona.address},${hosts.musi.address}";
        rpc_authentication_required = false;
      };
    };

    services.reaction.settings.patterns.ip.ignore = [ hosts.sona.address ];

    networking.firewall.extraCommands = ''
      iptables -A nixos-fw -p tcp --dport 9091 -s ${host.address}/24 -j nixos-fw-accept
    '';

    environment.systemPackages = [
      pkgs.stig
    ];
  }
]
