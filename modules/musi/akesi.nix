{ config, lib, pkgs, ... }:
let
  hostName = config.networking.hostName;
  hosts = builtins.fromTOML (builtins.readFile ../common/hosts.toml);
  host = hosts.${hostName};
in {
  services.nfs.server = {
    enable = true;
    # Doc: https://www.man7.org/linux/man-pages/man5/exports.5.html
    exports = ''
      /data/akesi ${hosts.akesi.address}(${lib.concatStringsSep "," [
        # Allow writes
        "rw"
        # Strong consistency
        "sync"
        # Docs says it causes more problems than solutions
        "no_subtree_check"
        # All requests are set to the following UID/GID
        "all_squash"
        "anonuid=${toString config.users.users."nfsakesi".uid}"
        "anongid=${toString config.users.groups."nfsakesi".gid}"
      ]})
    '';
    hostName = host.address;
  };

  users = {
    users = let
      eg = { extraGroups = [ "nfsakesi" ]; };
    in {
      ppom = eg;
      bertille = eg;
      media = eg;

      nfsakesi = {
        uid = 70;
        group = "nfsakesi";
      };
    };
    groups.nfsakesi.gid = 70;
  };

  environment.systemPackages = [ pkgs.stig ];

  systemd.paths.nfs-chown = {
    wantedBy = [ "multi-user.target" ];
    description = "Ensure nfs files have right owner";
    pathConfig = {
      PathChanged = "/data/akesi/dot.torrents";
      # Max one time per 4s
      TriggerLimitIntervalSec = "4s";
      TriggerLimitBurst = 1;
    };
  };
  systemd.services.nfs-chown = {
    description = "Ensure nfs files have right owner";
    serviceConfig = {
      # We sleep 5s to ensure we chown files changed after TriggerLimit
      ExecStartPre = [ "${pkgs.coreutils}/bin/sleep 5" ];
      ExecStart = "${pkgs.coreutils}/bin/chown -R nfsakesi:nfsakesi /data/akesi/dot.torrents";
    };
  };

  # Only allow akesi to connect to the NFS server via its Wireguard IP
  # Accept both udp and tcp
  # Also allow access to the binary cache
  networking.firewall.extraCommands = ''
    # Allow NFS for akesi
    iptables -A nixos-fw -p udp --dport 2049 -s ${hosts.akesi.address} -j nixos-fw-accept
    iptables -A nixos-fw -p tcp --dport 2049 -s ${hosts.akesi.address} -j nixos-fw-accept
    # Allow nix-serve for akesi & poki
    iptables -A nixos-fw -p tcp --dport ${builtins.toString config.services.nix-serve.port} -s ${hosts.akesi.address} -j nixos-fw-accept
    iptables -A nixos-fw -p tcp --dport ${builtins.toString config.services.nix-serve.port} -s ${hosts.poki.address} -j nixos-fw-accept
  '';

  services.reaction.settings.patterns.ip.ignore = [ hosts.akesi.address hosts.poki.address ];

  services.nix-serve = {
    enable = true;
    bindAddress = host.address;
    port = 4977;
    secretKeyFile = "/var/secrets/binarycache/key";
  };

  nix.settings.allowed-users = [ "nix-serve" ];
}
