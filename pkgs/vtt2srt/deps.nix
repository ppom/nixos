# file generated from go.mod using vgo2nix (https://github.com/nix-community/vgo2nix)
[
  {
    goPackagePath = "github.com/rzumer/vtt2srt";
    fetch = {
      type = "git";
      url = "https://github.com/rzumer/vtt2srt";
      rev = "e0c6a9c45886";
      sha256 = "0k2c72rjwq1pz7vq7zbkzx3whsg5x41f45swlmg1s5mxja55g7if";
      moduleDir = "";
    };
  }
]
